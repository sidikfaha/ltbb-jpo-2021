@extends('layouts.shop')

@section('title')
    Details sur le produit
@endsection

@section('stylesheet')
    <link rel="stylesheet" href="{{ asset("css/show.css") }}">
@endsection

@section('content')
    <div class="container-lg container-fluid">

        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrump">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="/">Accueil</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="/categories/{{ $product->category->id }}">{{ $product->category->name }}</a>
                        </li>
                        <li class="breadcrumb-item actice">
                            {{ $product->name }}
                        </li>
                    </ol>
                </nav>
            </div>

            <div class="col-md-6">
                <img style="width: 100%;" id="mainImg" src="{{ asset('storage') . '/' . $product->photo }}" alt="Profile">
            </div>

            <div class="col-md-6 d-flex flex-column justify-content-between">

                <div class="row">
                    <div class="col-12">

                        <h2 class="tumb font-weight-bold text-dark">{{ $product->name }}</h2>

                        <p class="text-muted">
                            {!! $product->description !!}
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Quos aliquid praesentium, ullam tempore harum sint itaque mollitia, dicta quaerat debitis impedit fugiat nihil, corporis facilis error! Magnam praesentium rerum reiciendis nemo ducimus! Ducimus ea, impedit amet at neque eum animi, vitae tempora tenetur suscipit, tempore et? Id velit distinctio dolorum.
                        </p>

                        <div class="row caracteristiques">
                            @foreach ($product->caracteristics as $caracteristic)
                                <div class="car tumb color-primary">
                                    <h5>{{ $caracteristic->name }}</h5>
                                    <p>{{ $caracteristic->value }}</p>
                                </div>
                            @endforeach
                        </div>

                        <div class="row pt-3 pb-3 text-muted">
                            <div class="col-md-2 ">
                                <div class="tumb">
                                    <i class="fa fa-star"></i> <strong>4.5</strong>
                                </div>
                            </div>

                            <div class="col-md-5">
                                <div>
                                    <i class="fa fa-store-alt"></i> <strong>50</strong> unités en stock
                                </div>
                            </div>

                            <div class="col-md-5">
                                <i class="fa fa-money-check"></i> <strong>0</strong> commandes
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-12 price">
                                <b>
                                    ~
                                    {{ number_format($product->price) }}
                                </b><span class="text-primary">Fcfa</span>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row order-box">

                    @if (!$found)
                        <div class="col-4">
                            <div class="row counter">
                                <div id="down" class="col-4 text-center btn-danger">
                                    <i class="fa fa-chevron-down"></i>
                                </div>
                                <div class="col-4" id="quantity">
                                    1
                                </div>
                                <div id="up" class="col-4 btn-primary">
                                    <i class="fa fa-chevron-up"></i>
                                </div>
                            </div>
                        </div>
                    @endif

                    <div class="col-lg-6 col-8">
                        <button @if ($found) disabled @endif data-price="{{ $product->price }}" data-article="{{ $product->id }}" data-token="{{ @csrf_token() }}" id="addToCart" class="btn @if ($found) cursor-denied @endif btn-success w-100">
                            @if ($found)
                                <i class="fa fa-cart-arrow-down"></i>
                                Déjà dans votre panier
                            @else
                                <i class="fas fa-cart-plus"></i>
                                Ajouter au panier
                            @endif
                        </button>
                    </div>

                </div>

            </div>

        </div>

        <div class="row py-5">

            <div class="col-md-6">
                <div class="row pl-5 text-center comments">
                    <i class="fa fa-comment-dots"></i>
                    &nbsp;
                    Avis
                </div>

                <hr>

                <form action="#" method="get" class="row user-comment pb-3">
                    <div class="col-12 text-center">
                        Votre commentaire
                    </div>

                    <div class="col-md-6 pt-1">
                        <label for="prenom"></label><input placeholder="Votre nom..." class="form-control" type="text" name="prenom" id="prenom">
                    </div>

                    <div class="col-md-6 pt-1">
                        <label for="nom"></label><input placeholder="Votre prenom..." class="form-control" type="text" name="nom" id="nom">
                    </div>

                    <div class="col-md-12 pt-3">
                        <label for="commentaire"></label><textarea placeholder="Votre commentaire..." class="form-control" name="commentaire" id="commentaire" rows="10"></textarea>
                    </div>

                    <div class="col-md-12 pt-3">
                        <button type="submit" class="btn btn-primary">
                            Envoyer
                        </button>
                    </div>

                </form>

            </div>

            <div class="col-md-6">

                <!-- the next section -->

            </div>

        </div>

        <div class="row">

            <div class="col-12 pt-md-5">
                <div class="row pl-5 text-center comments">
                    <i class="fa fa-comment-dots"></i>
                    &nbsp;
                    Avis des utilisateurs
                </div>

                <hr>
            </div>

            <div class="col-12">
                <div id="commentContainer" class="row">
                    <div class="col-12 alert alert-info"> Chargement ...</div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script>
        @if (!$found)
            $(() => {
                const up = $('#up')
                const down = $('#down')
                const qty = $('#quantity')

                up.click(() => {
                    qty.html( parseInt(qty.html()) + 1 )
                })

                down.click(() => {
                    const v = parseInt(qty.html())
                    qty.html( v > 1 ? v - 1 : 1)
                })
                App.addToCartInit()
            })
        @endif
    </script>
@stop
