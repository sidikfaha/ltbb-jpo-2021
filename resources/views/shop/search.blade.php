@extends('layouts.shop')

@section('title')
    Bienvenue dans notre boutique
@endsection

@section('content')
    <div class="container">

        <!-- Product and left menu  -->
        <div class="row">
            <!-- Left menu if >= col-md -->
            <div class="col-md-4 col-lg-3 d-none d-md-block">
                <div class="row sticky-top left-menu">
                    <div class="loader-content">
                        <div class="circle">
                            <div class="loader">
                            </div>
                        </div>
                    </div>
                    @include('partials.left-menu', compact('parents'))
                </div>
            </div>

            <!-- Product list -->
            <div id="product-list" class="col-md-8 col-lg-9 col-12">
                @include('partials._search', ['articles', 'href', 'rcat'])
            </div>
        </div>
    </div>
@endsection

@section('stylesheet')
    <link rel="stylesheet" href="{{ asset("css/shop.css") }}">
@endsection

@section('js')
    <script>
        $(function () {

            App.search()

            $('.loader-content').fadeOut(200)
            const chkd = JSON.parse($('#checked').html());
            const cats = document.querySelectorAll('.category-item')

            if (chkd) {
                cats.forEach(el => {
                    if (Utils.inArray(el.value, chkd)) {
                        console.log(el)
                        el.checked = true
                    }
                })
            }
        })
    </script>
@endsection
