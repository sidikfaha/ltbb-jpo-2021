@extends('layouts.shop')

@section('title')
  Votre panier
@endsection

@section('content')
  <div class="container-fluid bg-secondary text-white breadcrumb-banner">

    <div class="row pt-md-5 justify-content-between">
      <div class="col-md-2">
        <h1>Panier</h1>
      </div>
      <div class="col-2">
        <h2 class="badge badge-info">
          {{ Session::get('line_count') }} {{ Session::get('line_count') > 1 ? 'articles' : 'seul article' }}
        </h2>
      </div>
    </div>

  </div>

  <div class="container">

    <div class="row">

      <div class="col-md-8">
        <div class="loader-content">
          <div class="circle">
            <div class="loader">
            </div>
          </div>
        </div>
        @include('partials.cart-table', compact(['lines','data']))

      </div>

      <div class="col-md-4 pt-3 pt-md-0">
        <h5>Details sur la commande</h5>
        <table class="table table-dark table-borderless  table-hover table-active rounded">
          <tr>
            <td>Quantité totale</td>
            <td>{{ $data['total'] }}</td>
          </tr>
          <tr>
            <td>Total</td>
            <td>{{ number_format($data['sum']) }} Fcfa</td>
          </tr>
          <tr>
            <td>Reductions</td>
            <td>0 Fcfa</td>
          </tr>
          <tr>
            <td>Frais de transport</td>
            <td>Gratuit</td>
          </tr>
        </table>
      </div>

    </div>

    <hr>

    <div class="row d-flex justify-content-end">

      <div class="col d-flex justify-content-end">
{{--        <div class="px-2">--}}
{{--          <button class="btn btn-outline-danger">--}}
{{--            <i class="fa fa-recycle"></i> <br>--}}
{{--            Vider le panier--}}
{{--          </button>--}}
{{--        </div>--}}

        <div class="px-2">
          <button id="refresh" class="btn btn-outline-info">
            <i class="fa fa-sync"></i><br>
            Actualiser
          </button>
        </div>

        <div class="px-2">
          <a role="button" href="{{ route('checkout') }}" id="refresh" class="btn btn-outline-success">
            <i class="fa fa-check"></i><br>
            Valider et commander
          </a>
        </div>
      </div>
    </div>

    <hr>

  </div>
  <div class="d-none" id="token">{{ csrf_token() }}</div>
@endsection

@section('js')
  <script>
    $(() => {
      $('.loader-content').fadeOut(200)
      App.cartInit()
    })
  </script>
@endsection
