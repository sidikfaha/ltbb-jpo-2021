@extends('layouts.shop')

@section('title')
    Bienvenue dans notre boutique
@endsection

@section('content')
    <div class="container-fluid bg-dark text-light">
        <div class="row py-5">
          <div class="col-md-6">
            <img class="w-100" src="{{ asset("/storage/home-landing.jpeg") }}" alt="Home landing image">
          </div>
            <div class="col-md-6">
                <h1>{{ env("APP_NAME") }}</h1>
                <h4>
                  Faites le tour, sélectionnez les articles qui vous intéressent, passez votre commande
                  et faites vous livrer dans les plus bref délais...
                </h4>
                <h2 class="text-center">
                  Nous sommes disponibles <span class="text-primary">24h/24 - 7j/7</span>
                </h2>
            </div>
        </div>
    </div>

    <div class="container">

        <!-- Product and left menu  -->
        <div class="row">
            <!-- Left menu if >= col-md -->
            <div class="col-md-4 col-lg-3 d-none d-md-block">
                <div class="row sticky-top left-menu">
                    <div class="loader-content">
                        <div class="circle">
                            <div class="loader">
                            </div>
                        </div>
                    </div>
                    @include('partials.left-menu', compact('parents'))
                </div>
            </div>

            <!-- Product list -->
            <div id="product-list" class="col-md-8 col-lg-9 col-12">
                <div class="loader-content">
                    <div class="circle">
                        <div class="loader">
                        </div>
                    </div>
                </div>
                <div class="row">
                @if ($articles->count() > 0)
                    @foreach ($articles as $article)
                        @include('partials.product', $article)
                    @endforeach

                @else
                    <div class="col-12">
                        <div class="alert alert-info text-center">
                            Aucun produit trouvé !
                        </div>
                    </div>
                @endif
                </div>

                <div class="row">
                    <div class="col-12 d-flex justify-content-center">
                        {{ $articles->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('stylesheet')
    <link rel="stylesheet" href="/css/shop.css">
@endsection

@section('js')
    <script>
        App.search()

        $(function () {
           $('.loader-content').fadeOut(200)
        })
    </script>
@endsection
