<div class="loader-content">
    <div class="circle">
        <div class="loader">
        </div>
    </div>
</div>
<div class="row">
    @if ($articles->count() > 0)
        @foreach ($articles as $article)
            @include('partials.product', $article)
        @endforeach

    @else
        <div class="col-12">
            <div class="alert alert-info text-center">
                Aucun produit trouvé !
            </div>
        </div>
    @endif
</div>

<div class="row">
    <div class="col-12 d-flex justify-content-center">
        {{ $articles->links() }}
    </div>
    <div class="d-none">
        <div id="href">{{ $href }}</div>
        <div id="checked">{{ json_encode($rcat) }}</div>
    </div>
</div>
