<h5 class="font-weight-bold h5">Articles dans le panier</h5>
@if ($data['count'] > 0)
    <table class="table table-borderless table-bordered rounded table-striped mw-100">
        <tr class="bg-dark text-white ">
            <th>Designation</th>
            <th class="text-center">Qte</th>
            <th class="text-right">Montant</th>
        </tr>

        @foreach($lines as $line)
            <tr>
                <td>{{ $line->article->name }}</td>
                <td class="text-center">{{ $line->quantity }}</td>
                <td class="text-right">{{ number_format($line->price) }} Fcfa</td>
            </tr>
        @endforeach
    </table>
@else
    <div class="row">
        <div class="col-12 alert alert-info">
            Il n'ya encore aucun produit dans votre panier !
        </div>
    </div>
@endif
