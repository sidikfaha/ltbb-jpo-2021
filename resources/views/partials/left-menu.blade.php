<form onsubmit="return false" data-route="{{ route('search') }}" id="left-menu-form" class="col-12">

    <div class="row">
        <div class="col-12 input-group">
            @csrf
            <input type="text" name="query" id="query" class="form-control input-group-prepend">
            <button id="search-btn" class="btn btn-primary input-group-append"><i class="fa fa-search"></i></button>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <ul class="list-group list">
                @foreach ($parents as $parent)
                    @if ($parent->children->count() > 0)
                        <li class="list-group-item border-0">
                            {{ $parent->name }}
                            <ul class="list-group">

                                @foreach ($parent->children as $child)
                                    @if ($child->articles->count() > 0)
                                        <li class="list-group-item list-group-item-action ">
                                            <input name="category[]" id="cat-{{ $child->name }}" type="checkbox" class="category-item" value="{{ $child->id }}">
                                            <label class="form-check-label" for="cat-{{ $child->name }}">{{ $child->name }}</label>
                                        </li>
                                    @endif
                                @endforeach

                            </ul>
                        </li>
                    @endif
                @endforeach
            </ul>
        </div>

    </div>

</form>
