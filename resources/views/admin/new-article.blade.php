@extends('layouts.admin')

@section('title')
    Ajoutez un nouveau produit
@endsection

@section('content')
    @if (Session::has('message'))
        <div class="row">
            <div class="col-12">
                <div class="alert alert-success text-center">
                    {{ Session::get('message') }}
                </div>
            </div>
        </div>
    @endif
    <form class="row">

        <div class="col-md-4">
            <div class="row">
                <div class="col-12">
                    <img src="/public/img/no-image.svg" alt="Product image" class="w-100">
                </div>
                <div class="col-12">
                    <div class="custom-file">
                        <label class="custom-file-label" for="photo">Selectionnez une image</label>
                        <input class="custom-file-input" type="file" name="photo" id="photo">
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-8">
            <div class="form-group">
                <label for="name">Nom du produit</label>
                <input type="text" name="name" id="name" class="form-control">
            </div>

            <div class="form-group">
                <label for="description">Description du produit</label>
                <textarea name="description" id="description" rows="8" class="form-control"></textarea>
            </div>
        </div>
    </form>
@endsection
