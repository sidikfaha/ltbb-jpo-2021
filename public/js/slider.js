"use strict";

function SliderContent (c) {
    this.e = document.querySelectorAll(c);
    this.active = 0;

    for (let i = 0; i < this.e.length; i++) {
        let ch = this.e[i].firstChild.nextSibling;
        console.log(ch);

        ch.addEventListener('mouseover',() => {
            clearInterval(time);
        });

        ch.addEventListener('mouseleave', () => {
            time = setInterval(interval, 5000);
        });
    }

    for (let i = 0; i < this.e.length; i++) {
        let li = document.createElement('li');
        li.setAttribute('data-number', i.toString());
        document.querySelector('.slider-count').appendChild(li);

        li.addEventListener('click', () => {
            clearInterval(time);
            this.setActive(i);
            time = setInterval(interval, 5000);
        });
    }

    this.setActive(0);
}

SliderContent.prototype.getNumber = function () {
    return this.e.length;
};

SliderContent.prototype.getActive = function () {
    return this.active;
};

SliderContent.prototype.setActive = function (index) {
    for (let j = 0; j < this.e.length; j++) {
        this.e[j].setAttribute('class', 'slider-content');
        document.querySelectorAll('.slider-count li')[j].removeAttribute('class');
    }
    this.e[index].setAttribute('class', 'slider-content slider-active');
    this.active = index;
    document.querySelectorAll('.slider-count li')[index].setAttribute('class', 'active');
};

let a = new SliderContent('.slider-content');
let interval = () => {

    let i = a.getActive();
    if (i >= (a.getNumber() - 1)) {
        a.setActive(0);
    } else {
        a.setActive(i + 1);
    }
    console.log('all is done...');
};
let time = setInterval(interval , 5000);
