class App {

    static activateBot(){
        const btn = document.querySelector('#bot-btn')
        const messenger = document.querySelector('.messenger')
        const closeBtn = messenger.querySelector('.close .btn')
        const message = document.querySelector('#msg-content')
        const sendBtn = document.querySelector('.bottom .btn')

        const sendMessage = (msg = '', sender = 'client') => {
            if (msg !== "") {
                document.querySelector(".messenger .body").innerHTML += `
            <div class="msg msg-${sender} msg-new">
                <p>${msg}</p>
            </div>
            `;
                message.value = "";
                messenger.querySelector('.body').scrollTo({top: 10000});
                messenger.querySelector(".body .msg-new").classList.remove("msg-new");
                setTimeout(() => {

                }, 1000);
            }
        }

        btn.onclick = () => {
            messenger.classList.add('messenger-opened')
            document.body.style.overflow = 'hidden'
        }

        closeBtn.onclick = () => {
            messenger.classList.remove('messenger-opened')
            document.body.style.overflow = 'auto'
        }

        sendBtn.onclick = () => {
            sendMessage(message.value)
        }

    }

    static search(){
        new Search()
    }


    /**
     * @var {HTMLButtonElement} btn
     * @return {Promise<void>}
     */
    static async addToCartInit() {
        const btn = document.querySelector('#addToCart')
        const cart_count = document.querySelector("#count")
        const orderBox = $('.order-box')
        const frm = new FormData()

        btn.addEventListener('click', async () => {
            const quantity = document.querySelector('#quantity').innerHTML
            const article = btn.getAttribute('data-article')
            const token = btn.getAttribute('data-token')
            const price = parseInt(btn.getAttribute('data-price')) * parseInt(quantity)


            frm.append('quantity', quantity)
            frm.append('article', article)
            frm.append('_token', token)
            frm.append('price', price.toString())

            let rep = await fetch('/add', {
                method: "POST",
                body: frm
            })
            let result = await rep.json()

            if (result.status === 0) {
                cart_count.innerHTML = result.count
                orderBox.html(`
                    <div class="col-lg-6 col-8">
                        <button disabled class="btn btn-success w-100 cursor-denied">
                            <i class="fa fa-cart-arrow-down"></i>
                            Déjà dans votre panier
                        </button>
                    </div>
                `)

                alert(result.message)
            } else {
                alert(`Quelque chose a mal tourné !`)
            }

        })

        console.log('initialised successfully !')
    }

    static async cartInit() {
        const qtes = document.querySelectorAll('.line-quantity')
        const refresh = $('#refresh')

        for (const el of qtes) {
            el.addEventListener('change', async _ => {
                const loader = $('.loader-content')
                loader.fadeIn(200)

                const frm = new FormData()
                const article = el.getAttribute('data-article')
                const token = $('#token').html()
                const quantity = el.value
                const Uprice = parseInt(el.getAttribute('data-price'))
                const price = parseInt(quantity) * Uprice

                frm.append('article', article)
                frm.append('quantity', quantity)
                frm.append('price', price.toString())
                frm.append('_token', token)

                console.log(price)

                const rep = await fetch('/line/update', {
                    method: 'POST',
                    body: frm
                })
                const response = await rep.json()

                if (response.status === 0) {
                    loader.fadeOut(200)
                } else {
                    alert(response.message)
                }
            })
        }

        refresh.click(_ => {
            window.location.reload()
        })

    }

    static checkoutInit() {
        /**
         * @property {HTMLFormElement} frm
         * @property {FormData} data
         */
        const frm = document.querySelector('#checkoutForm')

        frm.addEventListener('submit', async e => {
            e.preventDefault()

            let data = new FormData(frm)
            const req = await fetch('/order/store', {
                method: "POST",
                body: data
            })
            const rep = await req.json()

            if (rep.status === 0) {
                alert(rep.message)
            } else {
                console.error(rep.message)
                console.error(rep)
            }
        })
    }
}

/**
 * @property {HTMLDivElement} content
 */
class Search {

    constructor() {
        this.items = document.querySelectorAll('.category-item')
        this.query = document.querySelector('#query')
        this.btn = document.querySelector('#search-btn')
        this.frm = $('#left-menu-form')
        this.content = document.querySelector('#produc-list')

        this.btn.addEventListener('click', evt => {
            this.find()
        })

        this.items.forEach(el => {
            el.addEventListener('change', () => {
                this.find()
            })
        })
    }

    // buildLink() {
    //     let result = 'categories='
    //
    //     this.items.forEach( i => {
    //         if (i.checked) {
    //             result += i.getAttribute('data-value')
    //             if( i !== this.items.lastChild){
    //                 result += encodeURIComponent(',')
    //             }
    //         }
    //
    //         i.addEventListener('change',  () => {
    //             this.find()
    //         })
    //     })
    //
    //     this.btn.addEventListener('click', () => {
    //         this.find()
    //     })
    //
    //     if (this.query.value !== '') {
    //         result += `&query=${encodeURIComponent(this.query.value)}`
    //     }
    //
    //     result += `&_token=${$('[name="_token"]').value}`
    //     console.log(result)
    //
    //     return result;
    // }

    find() {
        console.log(this.frm.serialize())

        $('.loader-content').fadeIn(200)

        $.ajax({
            url: this.frm.data('route'),
            type: "get",
            data: this.frm.serialize() + '&ajax=1',
            success: (r) => {
                document.querySelector('#product-list').innerHTML = r
                history.replaceState({}, '', `/search?${this.frm.serialize()}`)
                $('.loader-content').fadeOut(200)
            }
        })
    }
}

class Utils {
    /**
     * Verify if a given v value is contained in a given array
     * @param  v The value to research
     * @param {[]} arr the data source
     * @return boolean
     */
    static inArray(v, arr) {
        let c = 0
        let r = false

        while (c < arr.length && !r) {
            r = v === arr[c]
            c++
        }
        return r
    }
}
