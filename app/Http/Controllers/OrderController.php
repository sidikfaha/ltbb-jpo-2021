<?php

namespace App\Http\Controllers;

use App\Models\Line;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return array|string
     * @throws \JsonException
     */
    public function store(Request $request)
    {

        $lines = Session::get('lines');
        $sum = 0;

        foreach ($lines as $line) {
            $sum += $line->price;
        }

        $order = new Order([
            'session_id' => session()->getId(),
            'client_first_name' => $request->post('first'),
            'client_last_name' => $request->post('last'),
            'client_mobile' => $request->post('tel'),
            'client_email' => $request->post('email'),
            'client_country' => $request->post('country'),
            'client_city' => $request->post('city'),
            'client_address1' => $request->post('addr1'),
            'client_address2' => $request->post('addr2'),
            'cash_to_pay' => $sum,
            'payment_method' => $request->post('method'),
        ]);

        if ($order->save()) {

            foreach ($lines as $line) {
                $_line = new Line([
                    'quantity' => $line['quantity'],
                    'article_id' => $line['article_id'],
                    'session_id' => $line['session_id'],
                    'price' => $line['price'],
                    'order_id' => $order->id,
                ]);
                $_line->save();
            }

            Session::put('lines', []);
            Session::put('line_count', 0);

            return json_encode([
                'status' => 0,
                'message' => 'Votre commande a été transmise avec succès !',
                'order' => $order,
                'lines' => $order->lines
            ], JSON_THROW_ON_ERROR);
        } else {
            return json_encode([
                'status' => 1,
                'message' => 'Une erreur est survenue lors de la transmission de votre commande... Veuillez reessayer !',
                'order' => $order
            ], JSON_THROW_ON_ERROR);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param Order $order
     * @return void
     */
    public function show(Order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Order $order
     * @return void
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Order $order
     * @return void
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Order $order
     * @return void
     */
    public function destroy(Order $order)
    {
        //
    }
}
