<?php

namespace App\Http\Controllers;

use App\Models\Line;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class LineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        $lines = Session::get('lines');
        $sum = 0;
        $total = 0;

//        dd($lines);

        foreach ($lines as $line) {
            $sum += $line->price;
            $total += $line->quantity;
        }

        $data = [
            'sum' => $sum,
            'total' => $total,
            'count' => Session::get('line_count')
        ];

        return view('shop.cart', compact(['lines', 'data']));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return void
     */
    public function create(Request $request)
    {

        Session::push('lines', new Line([
            'quantity' => $request->post('quantity'),
            'article_id' => $request->post('article'),
            'session_id' => session()->getId(),
            'price' => $request->post('price')
        ]));

        Session::put('line_count', Session::get('line_count') + 1);

        $response = [
            'status' => 0,
            'message' => "L'article a bien été ajouté !",
            'count' => Session::get('line_count'),
            'lines' => Session::get('lines')
        ];

        return json_encode($response, JSON_PRETTY_PRINT);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Line  $line
     * @return \Illuminate\Http\Response
     */
    public function show(Line $line)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Line  $line
     * @return \Illuminate\Http\Response
     */
    public function edit(Line $line)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Line  $line
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Line $line)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Line  $line
     * @return \Illuminate\Http\Response
     */
    public function destroy(Line $line)
    {
        //
    }

    public function _update (Request $request) {
        $article = $request->post('article');
        $found = false;
        $count = 0;
        $lines = Session::get('lines');
        $line_count = Session::get('line_count');

        while (!$found && $count < $line_count ) {
            if ($lines[$count]['article_id'] === $article) {
                $lines[$count]['quantity'] = $request->post('quantity');
                $lines[$count]['price'] = $request->post('price');
                $found = true;
            } else {
                $count++;
            }
        }

        if ($found) {
            Session::put('lines', $lines);
            $data = [
                'status' => 0,
                'message' => 'La commade a bien été modifiée !',
                'lines' => Session::get('lines')
            ];
        } else {
            $data = [
                'status' => 1,
                'message' => 'Cet article n\'apparait pas dans votre pannier'
            ];
        }

        return json_encode($data);
    }

    public function _remove($article) {
        $lines = Session::get('lines');
        $nLines = [];

        foreach ($lines as $line) {
            if ($line['article_id'] !== $article){
                array_push($nLines, $line);
            }
        }

        Session::put('lines', $nLines);
        Session::put('line_count', sizeof($nLines));

        return back();
    }
}
