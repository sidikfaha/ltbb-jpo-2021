<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use HasFactory;

    public function category(){
        return $this->belongsTo(Category::class);
    }

    public function photos(){
        return $this->hasMany(Photo::class);
    }

    public function caracteristics() {
        return $this->hasMany(Caracteristic::class);
    }

    public function lines() {
        return $this->hasMany(Line::class);
    }
}
