<?php

use App\Http\Controllers\SearchController;
use TCG\Voyager\Facades\Voyager;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LineController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CheckoutController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [ArticleController::class,'index'])->name('home');
Route::get('/details/{article}', [ArticleController::class,'show']);
Route::get('/categories/{category}', [CategoryController::class,'index']);
Route::get('/cart', [LineController::class,'index'])->name('cart');
Route::get('/line/remove/{article}', [LineController::class,'_remove']);
Route::get('/checkout', [CheckoutController::class,'index'])->name('checkout');

// ajax
Route::get('/search', [SearchController::class,'index'])->name('search');
Route::post('/add', [LineController::class,'create'])->name('add');
Route::post('/line/update', [LineController::class,'_update']);
Route::post('/order/store', [OrderController::class,'store'])->name('store');



Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
